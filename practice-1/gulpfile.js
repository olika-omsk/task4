'use strict';

const { series } = require('gulp');

// Подключение плагинов
var gulp = require('gulp'),
    concat = require('gulp-concat'),
    clean1 = require('gulp-clean');

// Пути для сборки
var path = {
    build: 'build',
    src: 'src',
    watch: 'watch',
    clean: 'build'
};

// Очистка папок и файлов
function clean() {
    return gulp.src(path.clean, {read: false})
        .pipe(clean1());
}

//Конкатенация css
function styles(done) {
    gulp.src(path.src+'/styles/*.css')
        .pipe(concat('style.css'))
        .pipe(gulp.dest(path.build+'/styles/'));

    done();
}

//Копирование index.html
function html(done) {
    gulp.src(path.src+'/templates/index.html')
        .pipe(gulp.dest(path.build));

    done();
}

exports.build1 = series(styles, html);
exports.default = series(clean, styles, html);